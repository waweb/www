---
stackbit_url_path: /blog/author/pstevenson
template: advanced
seo:
  extra:
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: website
      keyName: property
      name: 'og:type'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Posts by Hilary Ouse
      keyName: property
      name: 'og:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: This is the author archive page
      keyName: property
      name: 'og:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/classic/post-5.png
      keyName: property
      name: 'og:image'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: summary_large_image
      keyName: name
      name: 'twitter:card'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Posts by Hilary Ouse
      keyName: name
      name: 'twitter:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: This is the author archive page
      keyName: name
      name: 'twitter:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/classic/post-5.png
      keyName: name
      name: 'twitter:image'
  type: stackbit_page_meta
  title: Posts by Hilary Ouse
  description: This is the author archive page
sections:
  - background_image_repeat: no-repeat
    background_image_position: center center
    type: hero_section
    background_image_size: cover
    background_color: none
    padding_bottom: none
    has_border: false
    media_width: fifty
    padding_top: medium
    align: center
    media_position: top
    actions: []
    title: Patrick Stevenson
    subtitle: COO/Co-Founder
  - type: blog_feed_section
    background_image_repeat: no-repeat
    background_image_position: center center
    background_image_size: cover
    background_color: none
    padding_top: small
    padding_bottom: large
    has_border: true
    show_image: true
    align: center
    show_author: true
    show_excerpt: true
    show_date: true
    show_categories: true
    author:
      photo_alt: Hilary Ouse
      type: person
      photo: 'https://www.datocms-assets.com/49813/1623787050-hugh-saturation.jpg'
      first_name: Patrick
      last_name: Stevenson
      id: pstevenson
      link: blog/author/pstevenson
    show_recent: false
    enable_cards: true
    blog_feed_cols: three
    actions: []
  - background_image_repeat: no-repeat
    type: form_section
    background_image_size: cover
    background_image_position: center center
    background_color: secondary
    has_border: true
    padding_bottom: medium
    form_fields:
      - type: form_field
        is_required: true
        default_value: Your email address
        label: Email
        input_type: email
        name: email
    padding_top: medium
    align_vert: top
    submit_label: Subscribe
    form_action: /thank-you
    form_id: subscribeForm
    form_width: fifty
    form_layout: inline
    enable_card: false
    form_position: bottom
    content: Subscribe to our newsletter to make sure you don't miss anything.
    title_align: center
    content_align: center
    title: Inline Form
title: Patrick Stevenson
---
