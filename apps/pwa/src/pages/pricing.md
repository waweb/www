---
stackbit_url_path: /pricing
template: advanced
seo:
  extra:
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: website
      keyName: property
      name: 'og:type'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Pricing
      keyName: property
      name: 'og:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: This is the pricing page
      keyName: property
      name: 'og:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/about-3.jpg
      keyName: property
      name: 'og:image'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: summary_large_image
      keyName: name
      name: 'twitter:card'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: Pricing
      keyName: name
      name: 'twitter:title'
    - type: stackbit_page_meta_extra
      relativeUrl: false
      value: This is the pricing page
      keyName: name
      name: 'twitter:description'
    - type: stackbit_page_meta_extra
      relativeUrl: true
      value: images/about-3.jpg
      keyName: name
      name: 'twitter:image'
  type: stackbit_page_meta
  title: Pricing
  description: This is the pricing page
sections:
  - background_image_repeat: no-repeat
    background_image_position: center center
    type: hero_section
    background_image_size: cover
    background_color: none
    padding_bottom: small
    has_border: false
    media_width: fifty
    padding_top: large
    align: center
    media_position: top
    actions: []
    title: Pick Your Plan
    subtitle: Optional hero section subtitle
  - type: grid_section
    background_image_repeat: no-repeat
    background_image_position: center center
    background_image_size: cover
    padding_bottom: medium
    background_color: none
    align: center
    has_border: false
    padding_top: small
    grid_gap_horiz: small
    enable_cards: true
    grid_gap_vert: small
    grid_cols: three
    grid_items:
      - type: grid_item
        image_has_padding: false
        image_align: left
        image_position: top
        image_width: fifty
        actions_width: full-width
        actions_align: left
        actions:
          - type: action
            icon_position: right
            no_follow: false
            new_window: false
            style: primary
            has_icon: false
            label: Start with Personal
            url: /features
        content_align: left
        title_align: left
        content: |-
          ---

          ### $9

          per month, billed yearly

          * Et semper leo mattis orci conubia congue
          * Quisque eleifend imperdiet tortor
          * Elementum eu rhoncus, volutpat
          * Nisi cubilia interdum ultricies
        subtitle: Optional subtitle
        title: Personal
      - type: grid_item
        image_has_padding: false
        image_align: left
        image_position: top
        image_width: fifty
        actions_width: full-width
        actions_align: left
        actions:
          - type: action
            icon_position: right
            no_follow: false
            new_window: false
            style: primary
            has_icon: false
            label: Start with Premium
            url: /features
        content_align: left
        title_align: left
        content: |-
          ---

          ### $19

          per month, billed yearly

          * Et semper leo mattis orci conubia congue
          * Quisque eleifend imperdiet tortor
          * Elementum eu rhoncus, volutpat
          * Fringilla diam magnis libero lacinia
          * Litora primis facilisis ullamcorper per
        subtitle: Optional subtitle
        title: Premium
      - type: grid_item
        image_has_padding: false
        image_align: left
        image_position: top
        image_width: fifty
        actions_width: full-width
        actions_align: left
        actions:
          - type: action
            icon_position: right
            no_follow: false
            new_window: false
            style: primary
            has_icon: false
            label: Start with Business
            url: /features
        content_align: left
        title_align: left
        content: |-
          ---

          ### $39

          per month, billed yearly

          * Et semper leo mattis orci conubia congue
          * Quisque eleifend imperdiet tortor
          * Elementum eu rhoncus, volutpat
          * Nisi cubilia interdum ultricies
          * Feugiat porta tortor
        subtitle: Optional subtitle
        title: Business
    actions: []
  - type: grid_section
    background_image_repeat: no-repeat
    background_image_position: center center
    background_image_size: cover
    padding_bottom: medium
    background_color: secondary
    align: center
    has_border: false
    padding_top: medium
    grid_gap_horiz: medium
    enable_cards: true
    grid_gap_vert: medium
    grid_cols: two
    grid_items:
      - type: grid_item
        image_has_padding: false
        image_align: left
        image_position: top
        image_width: fifty
        actions_width: auto
        actions_align: left
        actions: []
        content_align: left
        title_align: left
        content: >-
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nisl
          ligula, cursus id molestie vel, maximus aliquet risus. Vivamus in nibh
          fringilla, fringilla.
        title: Lorem ipsum dolor sit amet consectetur?
      - type: grid_item
        image_has_padding: false
        image_align: left
        image_position: top
        image_width: fifty
        actions_width: auto
        actions_align: left
        actions: []
        content_align: left
        title_align: left
        content: >-
          Ac felis donec et odio pellentesque. Sagittis vitae et leo duis ut
          diam quam nulla. Ullamcorper a lacus vestibulum sed arcu non odio
          euismod lacinia.
        title: Sagittis vitae et leo duis ut diam?
      - type: grid_item
        image_has_padding: false
        image_align: left
        image_position: top
        image_width: fifty
        actions_width: auto
        actions_align: left
        actions: []
        content_align: left
        title_align: left
        content: >-
          In tellus integer feugiat scelerisque. Aliquam eleifend mi in nulla
          posuere. Bibendum neque egestas congue quisque egestas. Mauris sit
          amet massa vitae tortor condimentum lacinia. Tortor at auctor urna
          nunc id cursus metus aliquam eleifend. Sed nisi lacus sed viverra
          tellus. Non enim praesent elementum facilisis.
        title: Viverra nam libero justo laoreet sit?
      - type: grid_item
        image_has_padding: false
        image_align: left
        image_position: top
        image_width: fifty
        actions_width: auto
        actions_align: left
        actions: []
        content_align: left
        title_align: left
        content: >-
          Blandit aliquam etiam erat velit. In massa tempor nec feugiat.
          Volutpat maecenas volutpat blandit aliquam. Sem integer vitae justo
          eget magna fermentum iaculis. Amet est placerat in egestas erat
          imperdiet sed euismod nisi. Facilisi morbi tempus iaculis urna.
        title: Cras tincidunt lobortis feugiat vivamus at augue eget arcu?
      - type: grid_item
        image_has_padding: false
        image_align: left
        image_position: top
        image_width: fifty
        actions_width: auto
        actions_align: left
        actions: []
        content_align: left
        title_align: left
        content: >-
          Facilisis gravida neque convallis a cras semper auctor neque vitae.
          Dictum varius duis at consectetur lorem donec massa. Porta non
          pulvinar neque laoreet suspendisse interdum consectetur libero.
        title: Porta nibh venenatis cras sed felis eget velit aliquet?
    actions: []
    title: 'You asked, we answered!'
title: Pricing
---
