---
stackbit_url_path: /thank-you
template: advanced
sections:
  - background_image_repeat: no-repeat
    background_image_position: center center
    type: hero_section
    background_image_size: cover
    background_color: none
    padding_bottom: large
    has_border: false
    media_width: fifty
    padding_top: medium
    align: center
    media_position: top
    actions:
      - type: action
        icon_position: right
        no_follow: false
        new_window: false
        style: primary
        has_icon: false
        label: Back to homepage
        url: /
    title: Thank You!
    subtitle: >-
      Thank you for contacting us. We will get back in touch with you soon. Have
      a great day!
title: Thank You
---
