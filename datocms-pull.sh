#!/usr/bin/env bash

set -e
set -o pipefail
set -v

echo "${0}: start build"

# fetch data from DatoCMS through datocms-pull
yarn --cwd apps/pwa \
  dlx @stackbit/datocms-pull \
  --ssg gatsby \
  --datocms-access-token \
  $DATOCMS_ACCESS_TOKEN

echo "${0}: finished build"
