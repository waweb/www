module.exports = {
  projects: [
    "<rootDir>/apps/pwa",
    "<rootDir>/apps/api",
    "<rootDir>/libs/utils",
    "<rootDir>/libs/hybrid-homepage",
  ],
  collectCoverage: true,
}
