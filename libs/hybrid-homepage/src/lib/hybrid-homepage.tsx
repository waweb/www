import "./hybrid-homepage.module.scss"

/* eslint-disable-next-line */
export interface HybridHomepageProps {}

export function HybridHomepage(props: HybridHomepageProps) {
  return (
    <div>
      <h1>Welcome to hybrid-homepage!</h1>
    </div>
  )
}

export default HybridHomepage
