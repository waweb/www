import { render } from "@testing-library/react"

import HybridHomepage from "./hybrid-homepage"

describe("HybridHomepage", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<HybridHomepage />)
    expect(baseElement).toBeTruthy()
  })
})
